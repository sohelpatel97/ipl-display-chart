'use strict'

// 1.Chart Of Total Matches Played Per Year

fetch('./output/totalMatchesPerYear.json').then((res)=>res.json()).then((data1)=>{

   ChartOfTotalMatchesPerYear(data1)
})

function ChartOfTotalMatchesPerYear(data1) {

    Highcharts.chart("container_1", {

      chart: {
        type: "column"
      },
      title: {
        text: "Total Matches Played That Year"
      },
      
      xAxis: {
        categories: Object.keys(data1),
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: "Total Matches"
        }
      }
  ,
      plotOptions: {
        column: {
          pointPadding: 0.25,
          borderWidth: 0
        }
      },
      series: [
        {
          name: "No. Of Matches Played Per Year",
          data: Object.values(data1)
        }
      ],
      
    });
  }


  // 2.totalWinsOfTeamYealy



fetch('./output/totalWinsOfTeamYealy.json').then((res)=>res.json()).then((data2)=>{

  ChartOfTotalWinsOfTeamYealy(data2)
})

function ChartOfTotalWinsOfTeamYealy(data) {


  let TeamDataObj = {};

  let YearCount = 0;

  for (let YearKey in data) {

    YearCount+=1;

    for (let TeamNameKey in data[YearKey]) {

      if (TeamNameKey in TeamDataObj) {

        TeamDataObj[TeamNameKey].push(data[YearKey][TeamNameKey]);

      } else {

        TeamDataObj[TeamNameKey] = [];

        for (let i = 1; i < YearCount; i++) {
          
          TeamDataObj[TeamNameKey].push(0);
        }
        TeamDataObj[TeamNameKey].push(data[YearKey][TeamNameKey]);
      }
    }
  }

  let TeamNameAndWins = [];

  for (let keyAsName in TeamDataObj) {
    
    let tempTeamObj = {};

    tempTeamObj["name"] = keyAsName;

    tempTeamObj["data"] = TeamDataObj[keyAsName];
    
    TeamNameAndWins.push(tempTeamObj);
  }




  Highcharts.chart('container_2', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total Wins Of Teams'
    },
    xAxis: {
        categories: Object.keys(data)
    },
    yAxis: {
        min: 0,
        title: {
            text: 'No. Of Wins '
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'gray'
            }
        }
    },
    // legend: {
    //     align: 'right',
    //     x: -30,
    //     verticalAlign: 'top',
    //     y: 25,
    //     floating: true,
    //     backgroundColor:
    //         Highcharts.defaultOptions.legend.backgroundColor || 'white',
    //     borderColor: '#CCC',
    //     borderWidth: 1,
    //     shadow: false
    // },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    series: TeamNameAndWins
});

 }

 //3 ExtraRunsConcededByTeam

 fetch('./output/ExtraRunsConcededByTeam.json').then((res)=>res.json()).then((data2)=>{

  ChartOfExtraRunsConcededByTeam(data2)
})

function ChartOfExtraRunsConcededByTeam(data) {

   Highcharts.chart("container_3", {

     chart: {
       type: "column"
     },
     title: {
       text: "Total Extra Runs Conceded By Team In 2016"
     },
     
     xAxis: {
       categories: Object.keys(data),
       crosshair: true
     },
     yAxis: {
       min: 0,
       title: {
         text: "Extra Runs"
       }
     }
 ,
     plotOptions: {
       column: {
         pointPadding: 0.25,
         borderWidth: 0
       }
     },
     series: [
       {
         name: "Extra Runs Conceded By Team In Year 2016",
         data: Object.values(data)
       }
     ],
     
   });
 }


 // 4.



 fetch('./output/Top10EconomicBowlers.json').then((res)=>res.json()).then((data3)=>{

  ChartOfTop10EconomicBowlers(data3)
})

function ChartOfTop10EconomicBowlers(data) {

   Highcharts.chart("container_4", {

     chart: {
       type: "column"
     },
     title: {
       text: "Top Ten Economics In 2015"
     },
     
     xAxis: {
       categories: data.map((ele)=>ele[0]),
       crosshair: true
     },
     yAxis: {
       min: 0,
       title: {
         text: "Economy"
       }
     }
 ,
     plotOptions: {
       column: {
         pointPadding: 0.25,
         borderWidth: 0
       }
     },
     series: [
       {
         name: " Bowlers Name ",
         data:  data.map((ele)=>ele[1]),
       }
     ],
     
   });
 }



  
  

